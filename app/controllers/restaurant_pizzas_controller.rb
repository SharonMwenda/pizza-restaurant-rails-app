class RestaurantPizzasController < ApplicationController
rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
    def create
        restaurantPizza = RestaurantPizza.create!(restaurantPizza_params)
        render json: restaurantPizza.pizza, status: :created
    end

    

    private 
    def restaurantPizza_params
        params.permit(:price, :pizza_id, :restaurant_id)
    end

    def render_unprocessable_entity_response(invalid)
        render json: {errors: invalid.record.errors}, status: :unprocessable_entity
    end
end
