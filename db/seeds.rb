# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)

restaurant1 = Restaurant.create(name: Faker::Restaurant.name, address: Faker::Address.street_address  )
restaurant2 = Restaurant.create(name: Faker::Restaurant.name, address: Faker::Address.street_address )
restaurant3 = Restaurant.create(name: Faker::Restaurant.name, address: Faker::Address.street_address  )

pizza1 = Pizza.create(name: 'Pepperoni', ingredients: Faker::Food.ingredient)
pizza2 = Pizza.create(name: 'Hawaiian', ingredients: Faker::Food.ingredient)
pizza3 = Pizza.create(name: 'BBQ Steak', ingredients: Faker::Food.ingredient)


RestaurantPizza.create(price: 5, restaurant_id: restaurant1.id, pizza_id: pizza1.id)
RestaurantPizza.create(price: 30, restaurant_id: restaurant1.id, pizza_id: pizza2.id)

RestaurantPizza.create(price: 8, restaurant_id: restaurant2.id, pizza_id: pizza1.id)
RestaurantPizza.create(price: 16, restaurant_id: restaurant2.id, pizza_id: pizza2.id)
RestaurantPizza.create(price: 27, restaurant_id: restaurant2.id, pizza_id: pizza3.id)

RestaurantPizza.create(price: 13, restaurant_id: restaurant3.id, pizza_id: pizza1.id)
RestaurantPizza.create(price: 15, restaurant_id: restaurant3.id, pizza_id: pizza3.id)


